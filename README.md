# Go-Rest-Demo

An example on go web service development

How to build **REST** API with http handlers in Golang

# How to start
```bash
go run .
```
or
```bash
docker build -t <tag> .
docker run -p 8080:8080 <tagname> 
```


In the following lines, you can find a short presentation.
---
---
---
# Start

- In Go, you need to think in modules, every module has its own directory
```bash
$ mkdir mydemo
$ cd mydemo
$ go mod init example.edu
```
---
# Language philosophy
- There's no such thing as **class**, you can encapsulate functions and/or data in **modules**
- Wants to be **C** without headaches: safer with error handling and garbage collection, no more weird type conversion, shorter code and verbose syntax
- **Concurrent programming**

---
# Example
```go
package example

import (
  "fmt"
  "errors"
)

func Greet(name string, age int) error {
  if age < 0 {
    return errors.New("age not valid")
  }
  fmt.Printf("Hello %v, you are %d years old.",name, age)
  return nil
} 
```


---
# Dependency tracking
- Has it's own "package manager"
- go.mod:
```go
module demo.edu/rest

go 1.16

require (
	github.com/julienschmidt/httprouter v1.3.0 // indirect
	github.com/manveru/faker v0.0.0-20171103152722-9fbc68a78c4d // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
)
```
---
# Dependency tracking
- go.sum:
```
github.com/davecgh/go-spew v1.1.1/go.mod h1:J7Y8YcW2NihsgmVo/mv3lAwl/skON4iLHjSsI+c5H38=
github.com/julienschmidt/httprouter v1.3.0 h1:U0609e9tgbseu3rBINet9P48AI/D3oJs4dN7jwJOQ1U=
github.com/julienschmidt/httprouter v1.3.0/go.mod h1:JR6WtHb+2LUe8TCKY3cZOxFyyO8IZAc4RVcycCCAKdM=
github.com/manveru/faker v0.0.0-20171103152722-9fbc68a78c4d h1:Zj+PHjnhRYWBK6RqCDBcAhLXoi3TzC27Zad/Vn+gnVQ=
github.com/manveru/faker v0.0.0-20171103152722-9fbc68a78c4d/go.mod h1:WZy8Q5coAB1zhY9AOBJP0O6J4BuDfbupUDavKY+I3+s=
...
```
- installing a module and adding to dependencies:
```bash
$ go get github.com/julienschmidt/httprouter
```
---
# How to create RESTful API
- My package of choice: github.com/julienschmidt/httprouter
- HttpRouter is a lightweight high performance HTTP request router (also called multiplexer or just mux for short) for Go
- Features:
  - Only explicit matches: With other routers, like ```http.ServeMux```, a requested URL path could match multiple patterns, therefore they have some awkward pattern priority rules
  - Stop parsing the requested URL path, just give the path segment a name and the router delivers the dynamic value to you

---
# Demo
- An example backend
- Mocked database
- Showcasing features of the package
- Showing how to dockerize it
