FROM alpine
ADD ./rest /go/bin/rest
EXPOSE "8081"
RUN mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2
CMD [ "/go/bin/rest" ]