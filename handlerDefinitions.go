package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/julienschmidt/httprouter"
	"github.com/sirupsen/logrus"
)

func GetUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	id, err := strconv.Atoi(p.ByName("id"))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		logrus.Error(err.Error())
		return
	}

	user, err := GetUserByID(id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound) // it's ok for now, since it can't throw an internal error cuz mocked
		fmt.Fprint(w, err.Error())
		return
	}

	if err := json.NewEncoder(w).Encode(user); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logrus.Error("Internal server error: could not parse result")
		return
	}

}

func PostUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	decoder := json.NewDecoder(r.Body)

	body := &User{}

	err := decoder.Decode(body)
	if err != nil {
		logrus.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	// "validation"
	if body.Email == "" || body.FirstName == "" || body.LastName == "" {
		logrus.Error("Invalid POST data")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	InsertUser(*body)

	w.WriteHeader(http.StatusAccepted)

}

func GetCity(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	id, err := strconv.Atoi(p.ByName("id"))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		logrus.Error(err.Error())
		return
	}

	city, err := GetCityByID(id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound) // it's ok for now, since it can't throw an internal error cuz mocked
		fmt.Fprint(w, err.Error())
		return
	}

	if err := json.NewEncoder(w).Encode(city); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logrus.Error("Internal server error: could not parse result")
		return
	}

}

func GetCitiesPaginated(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	queryParams := r.URL.Query()
	page, err := strconv.Atoi(queryParams.Get("page"))
	if err != nil {
		w.WriteHeader(http.StatusRequestedRangeNotSatisfiable)
		return
	}
	nrOfItems, err := strconv.Atoi(queryParams.Get("nrOfItems"))
	if err != nil {
		w.WriteHeader(http.StatusRequestedRangeNotSatisfiable)
		return
	}

	result, err := GetCities(page, nrOfItems)
	if err != nil {
		w.WriteHeader(http.StatusRequestedRangeNotSatisfiable)
		return
	}

	if err := json.NewEncoder(w).Encode(result); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		logrus.Error("Internal server error: could not parse result")
		return
	}
}
