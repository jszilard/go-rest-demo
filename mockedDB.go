package main

import (
	"errors"
	"log"
	"math"

	"github.com/manveru/faker"
)

type City struct {
	CityName    string
	CountryName string
	ZipCode     string
}

type User struct {
	FirstName string
	LastName  string
	Email     string
}

var cities []City
var users []User

func GenerateData(size int) {
	generator, err := faker.New("en")
	if err != nil {
		log.Fatal(err.Error())
	}

	for i := 0; i < size; i++ {

		users = append(users, User{
			FirstName: generator.FirstName(),
			LastName:  generator.LastName(),
			Email:     generator.Email(),
		})
		cities = append(cities, City{
			CityName:    generator.City(),
			CountryName: generator.Country(),
			ZipCode:     generator.PostCode(),
		})
	}
}

func GetCityByID(id int) (*City, error) {
	if id >= len(cities) || id < 0 {
		return nil, errors.New("not found")
	}
	return &cities[id], nil
}

func GetUserByID(id int) (*User, error) {
	if id >= len(users) || id < 0 {
		return nil, errors.New("not found")
	}
	return &users[id], nil
}

func InsertUser(u User) {
	users = append(users, u)
}

func InsertCity(c City) {
	cities = append(cities, c)
}

func GetCities(page int, numberOfElements int) ([]City, error) {
	if page < 1 || numberOfElements < 0 || (page-1)*numberOfElements > len(cities) {
		return nil, errors.New("not found")
	}

	end := int(math.Min(float64(len(cities)), float64(page*numberOfElements-1)))

	var res []City
	for i := (page - 1) * numberOfElements; i < end; i++ {
		res = append(res, cities[i])
	}

	return res, nil
}
