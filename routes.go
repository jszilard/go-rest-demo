package main

import (
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/sirupsen/logrus"
)

func Logger(next httprouter.Handle) httprouter.Handle {
	return httprouter.Handle(func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		logrus.SetFormatter(&logrus.TextFormatter{TimestampFormat: "2006-01-02 15:04:05", FullTimestamp: true})
		logrus.Printf("%+v %+v", r.Method, r.URL)

		/* since we always log, might just set CORS related stuff here */
		header := w.Header()
		header.Set("Access-Control-Allow-Methods", "POST,GET,DELETE")
		header.Set("Access-Control-Allow-Origin", "*")

		next(w, r, p)
	})
}

func CreateRoutes() {
	router := httprouter.New()

	router.GET("/users/:id", Logger(GetUser))
	router.POST("/users", Logger(PostUser))

	router.GET("/cities/:id", Logger(GetCity))
	router.GET("/cities/", Logger(GetCitiesPaginated))

	log.Default().Print("Listening on port 8081")
	log.Fatal(http.ListenAndServe(":8081", router))
}
