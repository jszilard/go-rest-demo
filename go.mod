module demo.edu/rest

go 1.16

require (
	github.com/julienschmidt/httprouter v1.3.0 // indirect
	github.com/manveru/faker v0.0.0-20171103152722-9fbc68a78c4d // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
)
